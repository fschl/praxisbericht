# Praxibericht

Frieder Schlesier

## EXIST Gründerstipendium OpenDriversLog

Westsächsische Hochschule Zwickau  
Fachbereich Physikalische Technik/Informatik  
Fachgruppe Informatik  

Technische Universität Freiberg

Sommer 2015

Mentor WHZ: Prof. Dr. Thomas Franke
Mentor TU Bergakademie Freiberg: Prof. Dr.-Ing. Berhnard Jung
Weisungsbefugter: B.Sc. Paul Petring

## Hinweise Installation LateX Windows

1. LaTeX installieren: http://miktex.org/download
2. Ghostscript (tex->dvi->pdf) installieren: http://ghostscript.com/download/
3. Latexeditor: http://www.xm1math.net/texmaker/download.html

Wenn Programme in richtiger Reihenfolge installiert wurden, so sollte _TexMaker_ selbst die korrekten Pfade zu den Latexprogrammen finden.


`main.tex` in texmaker öffnen und `Schnelles Übersetzen` `F1` ausprobieren.
Falls diese Fehlermeldung auftreten sollte:

    LaTeX Error: File `glossary-hypernav.sty' not found.

sollten die folgenden Schritte helfen:

1. Start-> All Programs -> MikTex 2.xx -> Maintenance(Admin) -> MikTex Package Manager (Admin)
2. folgende Pakete nachinstallieren:
    - glossary
    - glossaries
    - glossary-german
    - und ähnliche Pakete die _gloss_ enthalten
    - außer sprachspezifische Pakete

